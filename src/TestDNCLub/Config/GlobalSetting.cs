﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestDNCLub.Config
{
    public class DataConnection
    {
        public string ConnectionString { get; set; }
    }
}
